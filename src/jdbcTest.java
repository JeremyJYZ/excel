import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;
 
 
public class jdbcTest {
    public void jdbcConn(String img_name,String retailer_name,String pm_type,String pm_policy,String return_policy,String pp_policy) throws Exception {
        Connection conn = null;
        String sql;
        PreparedStatement pstmt=null;
        String url = "jdbc:mysql://localhost:3306/cpdb?"
                + "user=root&password=cp888&useUnicode=true&characterEncoding=UTF8";
        try {
            Class.forName("com.mysql.jdbc.Driver");
 
            System.out.println("成功加载MySQL驱动程序");
            conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            int result;
            sql = "insert into all_retailers(img_name,retailer_name,pm_type,pm_policy,return_policy,pp_policy) values(?,?,?,?,?,?)";
            pstmt=(PreparedStatement) conn.prepareStatement(sql);
 //           result = stmt.executeUpdate(sql);
            if(img_name!=null)
            	pstmt.setString(1,img_name); 
            else
            	pstmt.setString(1,null);
            if(retailer_name!=null)
            	pstmt.setString(2,retailer_name); 
            else
            	pstmt.setString(2,null);
            if(pm_type!=null)
            	pstmt.setString(3,pm_type); 
            else
            	pstmt.setString(3,null);
            if(pm_policy!=null)
            	pstmt.setString(4,pm_policy);
            else
            	pstmt.setString(4,null);
            if(return_policy!=null)
            	pstmt.setString(5,return_policy); 
            else
            	pstmt.setString(5,null);
            if(pp_policy!=null)
            	pstmt.setString(6,pp_policy); 
            else
            	pstmt.setString(6,null);
            pstmt.execute();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
 
    }
 
}