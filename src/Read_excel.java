import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

public class Read_excel{

    public void read_file() {
            try {
//            	String path = Read_excel.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"t.xls";
            	URL path = this.getClass().getResource("");
            	System.out.println(path.toString().substring(6)+"\\t.xls");
                FileInputStream excelFile = new FileInputStream(new File(path.toString().substring(6))+"\\Price policy lite UPDATE 20170412.xlsx");
                Workbook workbook = new XSSFWorkbook(excelFile);
                Sheet datatypeSheet = workbook.getSheetAt(0);
                Iterator<Row> iterator = datatypeSheet.iterator();
                
                while (iterator.hasNext()) {
                	String img_name = "";
                    String retailer_name = "";
                    String pm_type = "3";
                    String pm_policy = "";
                    String return_policy = "";
                    String pp_policy = "";
                    jdbcTest jdbcConn = new jdbcTest();
                    Row currentRow = iterator.next();
                    for(int i = 0;i<6;i++){
                    	Cell currentCell = currentRow.getCell(i);
                        //getCellTypeEnum shown as deprecated for version 3.15
                        //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==0) {
                        	img_name = currentCell.getStringCellValue();
//                            System.out.println("img_name ="+currentCell.getStringCellValue());
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==0) {
                        	img_name = String.valueOf((int)currentCell.getNumericCellValue());
//                            System.out.println("img_name ="+currentCell.getNumericCellValue());
                        }
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==1) {
                        	retailer_name = currentCell.getStringCellValue();
//                            System.out.println("retailer_name ="+currentCell.getStringCellValue());
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==1) {
                        	retailer_name = String.valueOf((int)currentCell.getNumericCellValue());
//                            System.out.println("retailer_name ="+currentCell.getNumericCellValue());
                        }
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==2) {
                        	pm_type = currentCell.getStringCellValue();
//                            System.out.println("pm_type ="+currentCell.getStringCellValue());
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==2) {
                        	pm_type = String.valueOf((int)currentCell.getNumericCellValue());
//                            System.out.println("pm_type ="+currentCell.getNumericCellValue());
                        }
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==3) {
                        	pm_policy = currentCell.getStringCellValue();
//                            System.out.println("pm_policy ="+currentCell.getStringCellValue());
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==3) {
                        	pm_policy = String.valueOf((int)currentCell.getNumericCellValue());
 //                           System.out.println("pm_policy ="+currentCell.getNumericCellValue());
                        }
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==4) {
                        	pp_policy = currentCell.getStringCellValue();
//                            System.out.println("return_policy ="+currentCell.getStringCellValue() + "--");
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==4) {
                        	pp_policy = String.valueOf((int)currentCell.getNumericCellValue());
 //                           System.out.println("return_policy ="+currentCell.getNumericCellValue() + "--");
                        }
                        if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.STRING&&i==5) {
                        	return_policy = currentCell.getStringCellValue();
//                            System.out.println("pp_policy ="+currentCell.getStringCellValue() + "--");
                        } else if (currentCell!=null&&currentCell.getCellTypeEnum() == CellType.NUMERIC&&i==5) {
                        	return_policy = String.valueOf((int)currentCell.getNumericCellValue());
//                            System.out.println("pp_policy ="+currentCell.getNumericCellValue() + "--");
                        }
                       
                    }
                    System.out.println("img_name: "+img_name);
                    System.out.println("retailer_name: "+retailer_name);
                    System.out.println("pm_type: "+pm_type);
                    System.out.println("pm_policy: "+pm_policy);
                    System.out.println("return_policy: "+return_policy);
                    System.out.println("pp_policy: "+pp_policy);
                    System.out.println("------------");
                    try {
						jdbcConn.jdbcConn(img_name, retailer_name, pm_type, pm_policy, return_policy, pp_policy);
					} catch (Exception e) {
						e.printStackTrace();
					}
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
    public static void main(String args[]){
    	Read_excel re = new Read_excel();
    	re.read_file();
//    	URL path = re.getClass().getResource("");
//    	System.out.println(path.toString().substring(6)+"\\t.xls");
    }
}